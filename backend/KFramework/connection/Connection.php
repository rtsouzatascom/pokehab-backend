<?php
namespace connection;

use PDO;

class Connection {
    
    public static $debug = true;
    
    //private static $db_user = "zeportei_usr_prd";
    //private  static $db_password = "L@r@L1zM@r1e";
    //private  static $db_name = "zeportei_db_prod";


    //private static $db_user = "zeportei_usr_dev";
   // private  static $db_password = "br@sil2019";
   // private  static $db_name = "zeportei_db_hml";

   //local Docker
   //  private static $db_user = "root";
   //  private  static $db_host = "192.168.15.10";

    // private static $db_user = "zeportei_usr_dev";
   //  private  static $db_password = "br@sil2019";
   //  private  static $db_name = "zeporteiro_dev";

    //private  static $db_host = "192.95.32.40";
    private static $db_host = "zeporteiro_dev.vpshost3752.mysql.dbaas.com.br";
    private static $db_user = "zeporteiro_db";
    private static $db_password = "br@sil2019DB";
    private static $db_name = "zeporteiro_db";

    
    private static $db;
    
    public static function getConnection(){

        /*
        if(Connection::$db == NULL)
            Connection::$db = new ezSQL_mysqli(Connection::$db_user, Connection::$db_password, Connection::$db_name, Connection::$db_host);
            
        Connection::$db->connect(Connection::$db_user, Connection::$db_password);
        */
        
        /*
        $mysqli = new mysqli(Connection::$db_host, Connection::$db_user,Connection::$db_password, Connection::$db_name);
        if ($mysqli->connect_errno) {
            echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
        }
        Connection::$db = $mysqli; 
        
        return Connection::$db;
        */
        return new PDO("mysql:host=".self::$db_host.";dbname=".self::$db_name, self::$db_user, self::$db_password,array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
          ));
    }
}