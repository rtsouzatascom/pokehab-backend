<?php
namespace control;

use control\KDAO;
use exception\NullArgException;

abstract class DAO implements KDAO
{

    protected abstract function checkKeys($function, $vo, $check_null, $check_type, $check_id);
    
    protected abstract function checkNullArg($function, $arg_nm, $arg);
    
    protected function checkNull($class, $function, $arg_nm, $arg){
        if($arg == NULL) throw new NullArgException("$arg_nm @ $class / $function");
        
    }

    protected abstract function checkType($obj);
    
}

?>
