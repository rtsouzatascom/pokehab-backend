<?php
namespace control;

interface KDAO {
    
    public function create($vo);
    public function retrieve($vo);
    public function update($vo);
    public function delete($vo);
    public function filter($vo);
    
    
}

