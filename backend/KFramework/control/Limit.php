<?php
class Limit {
    
    public static $_LIMIT = "limit";
    public static $_OFFSET = "offset";
    
    private $limit; //quantos
    private $offset; //a partir deste
    
    public function __construct($limit, $offset=null){
        $this->limit = $limit;
        $this->offset = $offset;
    }
    
    public function getLimit(){
        return $this->limit;
    }
    
    public function hasOffset(){
        return isset($this->offset) && !is_null($this->offset);
    }
    
    public function getOffset(){
        return $this->offset;
    }
    
}