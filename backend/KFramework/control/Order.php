<?php
class Order {
    
    public static $_ASC = "ASC";
    public static $_DESC = "DESC";
    
    private $field;
    private $type;
    
    public function __construct($field, $type){
        $this->field = $field;
        $this->type = $type;
    }
    
    public function getField(){
        return $this->field;
    }
    
    public function getType(){
        return $this->type;
    }
    
}