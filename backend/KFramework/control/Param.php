<?php
namespace control;

use PDO;

class Param {
    
    public static $_STRING = PDO::PARAM_STR;
    public static $_INTEGER = PDO::PARAM_INT;
    public static $_TIMESTAMP = PDO::PARAM_INT;
    
    public static $_EQUAL = "=";
    public static $_NOT_EQUAL = "<>";
    public static $_GT = ">";
    public static $_GToE = ">=";
    public static $_LT = "<";
    public static $_LToE = "<=";
    public static $_IS_NULL = " IS NULL ";
    public static $_IS_NOT_NULL = " IS NOT NULL ";
    public static $_EMPTY = "";
    public static $_BETWEEN = "BETWEEN";
    public static $_IN = "IN";
    
    private $type;
    private $field;
    private $value;
    private $op;
    private $is_key;
    private $id = "";
    
    public function __construct($type, $field, $value, $is_key, $op, $id){
        $this->type = $type;
        $this->field = $field;
        $this->value = $value;
        $this->op = $op;
        $this->is_key = $is_key;
        $this->id = $id;
    }
    
    public function getId(){
        return $this->id;
    }

    public function getType(){
        return $this->type;
    }
    
    public function getField(){
        return $this->field;
    }
    
    public function getValue(){
        return $this->value;
    }
    
    public function getOp() {
        return $this->op;
    }
    
    public function isKey(){
        return $this->is_key;
    }
}

