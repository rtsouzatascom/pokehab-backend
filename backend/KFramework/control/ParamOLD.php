<?php
namespace control;

class Param {
    
    public static $_STRING = 's';
    public static $_INTEGER = 'i';
    
    public const _EQUAL = "=";
    public const _GT = ">";
    public const _GToE = ">=";
    public const _LT = "<";
    public const _LToE = "<=";
    
    private $type;
    private $field;
    private $value;
    private $op;
    private $is_key;
    
    public function __construct($type, $field, $value, $is_key, $op){
        $this->type = $type;
        $this->field = $field;
        $this->value = $value;
        $this->op = $op;
        $this->is_key = $is_key;
    }
    
    public function getType(){
        return $this->type;
    }
    
    public function getField(){
        return $this->field;
    }
    
    public function getValue(){
        return $this->value;
    }
    
    public function getOp() {
        return $this->op;
    }
    
    public function isKey(){
        return $this->is_key;
    }
}

