<?php
namespace control;

class ParamSet {
    
    private $param_set = array();
    
    public function addParam($type, $field, $value, $is_key, $op){
        if(empty($op)) $op = Param::$_EQUAL;
        $id = sizeof($this->param_set);
        array_push($this->param_set, new Param($type, $field, $value, $is_key, $op, $id));
    }
    
    public function addRange($type, $field, $begin, $end, $inclusive){
        $op_begin = ($inclusive) ? Param::$_GToE : Param::$_GT;
        $op_end = ($inclusive) ? Param::$_LToE : Param::$_LT;
        
        $this->addParam($type, $field, $begin, $is_key, $op_begin);
        $this->addParam($type, $field, $end, $is_key, $op_end);

    }

    public function getTypesString(){
        $types = "";
        foreach ($this->param_set as $i => $param) {
            $types = $types.($param->getType());
        }
        return $types;
    }
    
    public function getValues(){
        $values = array();
        foreach ($this->param_set as $i => $param) {
            array_push($values, htmlspecialchars(($param->getValue())));
        }
        return $values;
    }
    
    public function getFields(){
        $fields = array();
        foreach ($this->param_set as $i => $param) {
            array_push($fields, ($param->getField()));
        }
        return $fields;
    }
   
    //return (field_name, field_name, field_name, field_name...)
    public function getFieldsString(){
        $field_str = "";
        $separator = "";
        foreach($this->param_set as $param){
            $field_str = $field_str.$separator.($param->getField());
            $separator = ", ";
        }
        return $field_str;
    }

    //return (?, ?, ?, ?...)
    public function getInsertParams(){
        $string = "";
        $separator = "";
        foreach ($this->param_set as $param){
            $string = $string.$separator.":".$param->getField();
            $separator = ", ";
        }
        return $string;
    }
    
    //return (field_name = ?, field_name = ?, field_name = ?, ...)
    public function getUpdateParams(){
        $string = "";
        $separator = "";
        foreach ($this->param_set as $param){
            if(!$param->isKey()){
                $field = $param->getField();
                $string = $string.$separator.$field." = :".$param->getField();
                $separator = ", ";
            }
        }
        return $string;
    }
    
    //return (field_name = ? AND field_name = ? AND field_name = ?, ...)
    public function getParamsConditions(){
        $string = "";
        $conjunction = "";
        foreach ($this->param_set as $param){
            if(!$param->isKey()){
                $field = $param->getField();
                $op = $param->getOp();

                $value = "";
                if($op != Param::$_IS_NULL)
                    $value = " :".$param->getField()."_".$param->getId();

                $string = $string.$conjunction.$field." $op ".$value;
                $conjunction = " AND ";
            }
        }
        return $string;
    }

    //return (?, ?, ?, ?...) only primary keys
    public function getKeysString(){
        $string = "";
        $separator = "";
        foreach ($this->param_set as $param){
            if($param->isKey()){
                $field = $param->getField();
                $string = $string.$separator.$field;
                $separator = ", ";
            }
        }
        return $string;
    }
    
    
    public function getKeysCondition(){
        $string = "";
        $separator = "";
        foreach ($this->param_set as $param){
            if($param->isKey()){
                $field = $param->getField();
                $op = $param->getOp();
                $string = $string.$separator.$field." $op :".$param->getField();
                $separator = " AND ";
            }
        }
        return $string;
    }
    
    
    public function size(){
        return count($this->param_set);
    }
    
    public function getParams(){
        return $this->param_set;
    }
}

