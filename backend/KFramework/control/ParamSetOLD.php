<?php
namespace control;

class ParamSet {
    
    private $param_set = array();
    
    public function addParam($type, $field, $value, $is_key, $op = Param::_EQUAL){
        array_push($this->param_set, new Param($type, $field, $value, $is_key, $op));
    }
    
    public function getTypesString(){
        $types = "";
        foreach ($this->param_set as $i => $param) {
            $types = $types.($param->getType());
        }
        return $types;
    }
    
    public function getValues(){
        $values = array();
        foreach ($this->param_set as $i => $param) {
            array_push($values, htmlspecialchars(($param->getValue())));
        }
        return $values;
    }
    
    public function getFields(){
        $fields = array();
        foreach ($this->param_set as $i => $param) {
            array_push($fields, ($param->getField()));
        }
        return $fields;
    }
    
    //return (field_name, field_name, field_name, field_name...)
    public function getFieldsString(){
        $field_str = "";
        $separator = "";
        foreach($this->param_set as $param){
            $field_str = $field_str.$separator.($param->getField());
            $separator = ", ";
        }
        return $field_str;
    }

    //return (?, ?, ?, ?...)
    public function getInsertParams(){
        $string = "";
        $separator = "";
        foreach ($this->param_set as $param){
            $string = $string.$separator."?";
            $separator = ", ";
        }
        return $string;
    }
    
    //return (field_name = ?, field_name = ?, field_name = ?, ...)
    public function getUpdateParams(){
        $string = "";
        $separator = "";
        foreach ($this->param_set as $param){
            if(!$param->isKey()){
                $field = $param->getField();
                $string = $string.$separator.$field." = ?";
                $separator = ", ";
            }
        }
        return $string;
    }
    
    //return (field_name = ? AND field_name = ? AND field_name = ?, ...)
    public function getParamsConditions(){
        $string = "";
        $conjunction = "";
        foreach ($this->param_set as $param){
            if(!$param->isKey()){
                $field = $param->getField();
                $op = $param->getOp();
                $string = $string.$conjunction.$field." $op ?";
                $conjunction = " AND ";
            }
        }
        return $string;
    }
    
    //return (?, ?, ?, ?...) only primary keys
    public function getKeysString(){
        $string = "";
        $separator = "";
        foreach ($this->param_set as $param){
            if($param->isKey()){
                $field = $param->getField();
                $string = $string.$separator.$field;
                $separator = ", ";
            }
        }
        return $string;
    }
    
    
    public function getKeysCondition(){
        $string = "";
        $separator = "";
        foreach ($this->param_set as $param){
            if($param->isKey()){
                $field = $param->getField();
                $op = $param->getOp();
                $string = $string.$separator.$field." $op ? ";
                $separator = " AND ";
            }
        }
        return $string;
    }
    
    
    public function size(){
        return count($this->param_set);
    }
}

