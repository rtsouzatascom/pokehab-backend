<?php
namespace control;

use connection\Connection;
use exception\NotFoundException;
use Limit;
use Order;
use PDO;

class Repository
{

    private static $db;

    private $_ENTITY;

    public function __construct($entity)
    {
        $this->_ENTITY = $entity;
    }

    public function insert(ParamSet $paramSet)
    {
        $_ENTITY = $this->_ENTITY;
        $_FIELDS = $paramSet->getFieldsString();
        $_PARAMS = $paramSet->getInsertParams();
        $insert = "INSERT INTO $_ENTITY ($_FIELDS) VALUES ($_PARAMS)";
        
        //@error_log($insert);
        $this->execute($insert, $paramSet);
       
        return true;
    }
    public function insertLast(ParamSet $paramSet)
    {
        $_ENTITY = $this->_ENTITY;
        $_FIELDS = $paramSet->getFieldsString();
        $_PARAMS = $paramSet->getInsertParams();
        $insert = "INSERT INTO $_ENTITY ($_FIELDS) VALUES ($_PARAMS)";
        
        //@error_log($insert);
        $this->execute($insert, $paramSet);
        $stmt = Repository::$db->lastInsertId();
        return $stmt;
    }
  
    public function update(ParamSet $paramSet)
    {
        //$servico->setDtDelete();
        $paramSet->addParam(Param::$_TIMESTAMP, 'dt_update', date("Y-m-d H:i:s"), FALSE, Param::$_EQUAL);

        $_ENTITY = $this->_ENTITY;
        $_KEYS = $paramSet->getKeysCondition();
        $_FIELDS = $paramSet->getFieldsString();
        $_PARAMS = $paramSet->getUpdateParams();
        

        $update = "UPDATE $_ENTITY SET $_PARAMS WHERE $_KEYS";
        //error_log($update);
        
        
        $this->execute($update, $paramSet);
        return true;
    }

    public function retrieve(ParamSet $paramSet)
    {
        $_ENTITY = $this->_ENTITY;
        $_CONDITION = $paramSet->getParamsConditions();
        
        $retrieve = "SELECT * FROM $_ENTITY WHERE $_CONDITION";
        
        //error_log($retrieve);
        
        return $this->fetch($retrieve, $paramSet);
    }

    public function select(ParamSet $paramSet, $order, $limit)
    {
        $_ENTITY = $this->_ENTITY;
        $_CONDITION = $paramSet->getParamsConditions();
        
        $select = "SELECT * FROM $_ENTITY";
        if($paramSet->size()) {
            $select = $select." WHERE $_CONDITION";
            
        } 
        if(isset($order)){
            $select = $select." ORDER BY ".$order->getField()." ".$order->getType();
        }
        if(isset($limit)){
            $select = $select." LIMIT ".$limit->getLimit();
            if($limit->hasOffset()) $select = $select." OFFSET ".$limit->getOffset();
        }
        
        //error_log("query:");
        //error_log($select);
        //error_log("args:");
        //error_log(print_r($paramSet, true));
       
        return $this->retrieve_set($select, $paramSet);
    }
    public function selectBetween(ParamSet $paramSet){
        
        $_ENTITY = $this->_ENTITY;
        $_FIELDS = $paramSet->getFields();
        $_VALUES = $paramSet->getValues();
        $select = "SELECT * FROM $_ENTITY";
        $select = $select." WHERE $_FIELDS[1] BETWEEN DATE('$_VALUES[1]') AND DATE('$_VALUES[2]') AND $_FIELDS[0] = $_VALUES[0] ";
    
        if (Repository::$db == null)
            Repository::init();
       
        $stmt = Repository::$db->prepare($select);
        $stmt->execute();
        
        $result_set = array();
        while ($array_obj = $stmt->fetch(PDO::FETCH_OBJ)) {
            array_push($result_set, $array_obj);
        }
        
        /* return */
        return $result_set;
        

    }
    public function selectIn(ParamSet $paramSet, ParamSet $param1){
        $_ENTITY = $this->_ENTITY;
        $_CONDITION = $paramSet->getParamsConditions();
        
        $select = "SELECT * FROM $_ENTITY";
        if($paramSet->size()) {
            $select = $select." WHERE $_CONDITION";
            
        } 
        $_ENTITY = $this->_ENTITY;
        $_FIELDS = $param1->getFields();
        $_VALUES = $param1->getValues();
        $select = $select." AND $_FIELDS[1] IN ($_VALUES[0],$_VALUES[1])";
      //  print_r($select);
        if (Repository::$db == null)
        Repository::init();

        $stmt = Repository::$db->prepare($select);
        foreach ($paramSet->getParams() as $param){

            if($param->getOp() != Param::$_IS_NULL)
                $stmt->bindValue(":".$param->getField()."_".$param->getId(), $param->getValue(), $param->getType());
        }
       // print_r($stmt);
       
        $stmt->execute();

        $result_set = array();
        while ($array_obj = $stmt->fetch(PDO::FETCH_OBJ)) {
            array_push($result_set, $array_obj);
        }
      
        return $result_set;
    }
    public function contains(ParamSet $paramSet){
        $_ENTITY = $this->_ENTITY;
        $_CONDITION = $paramSet->getParamsConditions();
        $select_count = "SELECT COUNT(*) FROM $_ENTITY WHERE $_CONDITION";
        return ($this->count($select_count, $paramSet) > 0);
        
    }
    
    public function selectCount($key, ParamSet $paramSet)
    {
        
        $_ENTITY = $this->_ENTITY;
        $_CONDITION = $paramSet->getParamsConditions();
        $select_count = "SELECT COUNT($key) FROM $_ENTITY WHERE $_CONDITION";
        
       
        
        return $this->count($select_count, $paramSet);
    }
   

    public function delete($paramSet)
    {
        $_ENTITY = $this->_ENTITY;
        $_KEYS = $paramSet->getKeysCondition();
        
        $delete = "DELETE FROM $_ENTITY WHERE $_KEYS";
        
        $this->execute($delete, $paramSet);
        return true;
    }

    private static function init()
    {
        $conn = Connection::getConnection();
        /*
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }*/
        
        Repository::$db = $conn;
    }

    private function retrieve_set($query, $paramSet)
    {
        if (Repository::$db == null)
            Repository::init();
            
        $stmt = Repository::$db->prepare($query);
        
        foreach ($paramSet->getParams() as $param){

            if($param->getOp() != Param::$_IS_NULL)
                $stmt->bindValue(":".$param->getField()."_".$param->getId(), $param->getValue(), $param->getType());
        }
        
        
        //@error_log(print_r($stmt, true));
        $stmt->execute();
        
        $result_set = array();
        while ($array_obj = $stmt->fetch(PDO::FETCH_OBJ)) {
            array_push($result_set, $array_obj);
        }
        
        /* return */
        return $result_set;
    }

    private function fetch($query, $paramSet)
    {
        
        $result = $this->retrieve_set($query, $paramSet);
        if (count($result)) {
            return $result[0];
        } else {
            throw new NotFoundException("");
        }
    }

    private function execute($query, $paramSet)
    {
        if (Repository::$db == null)
            Repository::init();
    
        $stmt = Repository::$db->prepare($query);
        
        //echo $query;
        //print_r($paramSet);
        
        /* bind params to prepared statement */
        //$this->bind_params($stmt, $paramSet);
        
        foreach ($paramSet->getParams() as $param){
            if($param->getOp() != Param::$_IS_NULL)
                $stmt->bindValue(":".$param->getField(), $param->getValue());
        }
        
        /* execute */
        
        //@error_log($stmt);
        $stmt->execute();
    }

    private function count($query, $paramSet)
    {
        if (Repository::$db == null)
            Repository::init();

        //echo $query;
        //se o statement for false, checar:
            //o nome da $_ENTITY está igual ao nome da tabela?

        $stmt = Repository::$db->prepare($query);
        
        /* bind params to prepared statement */
        //$this->bind_params($stmt, $paramSet);
        foreach ($paramSet->getParams() as $param){
            if($param->getOp() != Param::$_IS_NULL)
                $stmt->bindValue(":".$param->getField()."_".$param->getId(), $param->getValue());
        }
        
        
        //print_r($stmt);
        
        /* execute */
        //@error_log($stmt);
        $stmt->execute();

        /* bind results to prepared statement */
        $count = $stmt->fetchColumn(); 

        /* return */
        return $count;
    }

    /*
    private function bind_params($stmt, ParamSet $paramSet)
    {
        
        $types = $paramSet->getTypesString();
        
        $values = $paramSet->getValues();
        
        switch ($paramSet->size()) {
            case 0:
                break;
            case 1:
                $stmt->bind_param($types, $values[0]);
                break;
            case 2:
                $stmt->bind_param($types, $values[0], $values[1]);
                break;
            case 3:
                $stmt->bind_param($types, $values[0], $values[1], $values[2]);
                break;
            case 4:
                $stmt->bind_param($types, $values[0], $values[1], $values[2], $values[3]);
                break;
            case 5:
                $stmt->bind_param($types, $values[0], $values[1], $values[2], $values[3], $values[4]);
                break;
            case 6:
                $stmt->bind_param($types, $values[0], $values[1], $values[2], $values[3], $values[4], $values[5]);
                break;
            case 7:
                $stmt->bind_param($types, $values[0], $values[1], $values[2], $values[3], $values[4], $values[5], $values[6]);
                break;
        }
    }
    

    private function bind_results($stmt)
    {
        $resultSet = array();
        
        if ($stmt->affected_rows > 0)
            switch ($stmt->field_count) {
                case 1:
                    $stmt->bind_result($resultSet[0]);
                    break;
                case 2:
                    $stmt->bind_result($resultSet[0], $resultSet[1]);
                    break;
                case 3:
                    $stmt->bind_result($resultSet[0], $resultSet[1], $resultSet[2]);
                    break;
                case 4:
                    $stmt->bind_result($resultSet[0], $resultSet[1], $resultSet[2], $resultSet[3]);
                    break;
                case 5:
                    $stmt->bind_result($resultSet[0], $resultSet[1], $resultSet[2], $resultSet[3], $resultSet[4]);
                    break;
                case 6:
                    $stmt->bind_result($resultSet[0], $resultSet[1], $resultSet[2], $resultSet[3], $resultSet[4], $resultSet[5]);
                    break;
                case 7:
                    $stmt->bind_result($resultSet[0], $resultSet[1], $resultSet[2], $resultSet[3], $resultSet[4], $resultSet[5], $resultSet[6]);
                    break;
        }
        
        return $resultSet;
    }
    */
}

