<?php
namespace control;

use connection\Connection;
use exception\NotFoundException;
use Limit;
use Order;

class RepositoryOLD
{

    private static $db;

    private $_ENTITY;

    public function __construct($entity)
    {
        $this->_ENTITY = $entity;
    }

    public function insert(ParamSet $paramSet)
    {
        $_ENTITY = $this->_ENTITY;
        $_FIELDS = $paramSet->getFieldsString();
        $_PARAMS = $paramSet->getInsertParams();
        $insert = "INSERT INTO $_ENTITY ($_FIELDS) VALUES ($_PARAMS)";
        
        $this->execute($insert, $paramSet);
        return true;
    }

    public function update(ParamSet $paramSet)
    {
        $_ENTITY = $this->_ENTITY;
        $_KEYS = $paramSet->getKeysCondition();
        $_FIELDS = $paramSet->getFieldsString();
        $_PARAMS = $paramSet->getUpdateParams();
        
        $update = "UPDATE $_ENTITY SET $_PARAMS WHERE $_KEYS";
        
        //echo $update;
        
        $this->execute($update, $paramSet);
        return true;
    }

    public function retrieve(ParamSet$paramSet)
    {
        $_ENTITY = $this->_ENTITY;
        $_CONDITION = $paramSet->getParamsConditions();
        
        $retrieve = "SELECT * FROM $_ENTITY WHERE $_CONDITION";
        return $this->fetch($retrieve, $paramSet);
    }

    public function select(ParamSet $paramSet, Order $order = null, Limit $limit = null)
    {
        $_ENTITY = $this->_ENTITY;
        $_CONDITION = $paramSet->getParamsConditions();
        
        $select = "SELECT * FROM $_ENTITY";
        if($paramSet->size()) {
            $select = $select." WHERE $_CONDITION";
            
        } 
        if(isset($order)){
            $select = $select." ORDER BY ".$order->getField()." ".$order->getType();
        }
        if(isset($limit)){
            $select = $select." LIMIT ".$limit->getLimit();
            if($limit->hasOffset()) $select = $select." OFFSET ".$limit->getOffset();
        }
        
        //echo $select;
        
        return $this->retrieve_set($select, $paramSet);
    }

    public function contains(ParamSet$paramSet){
        $_ENTITY = $this->_ENTITY;
        $_CONDITION = $paramSet->getParamsConditions();
        $select_count = "SELECT COUNT(*) FROM $_ENTITY WHERE $_CONDITION";
        return ($this->count($select_count, $paramSet) > 0);
        
    }
    
    public function selectCount($key, ParamSet $paramSet)
    {
        $_ENTITY = $this->_ENTITY;
        $_CONDITION = $paramSet->getParamsConditions();
        $select_count = "SELECT COUNT($key) FROM $_ENTITY WHERE $_CONDITION";
        
        return $this->count($select_count, $paramSet);
    }

    public function delete($paramSet)
    {
        $_ENTITY = $this->_ENTITY;
        $_KEYS = $paramSet->getKeysCondition();
        
        $delete = "DELETE FROM $_ENTITY WHERE $_KEYS";
        
        $this->execute($delete, $paramSet);
        return true;
    }

    private static function init()
    {
        $conn = Connection::getConnection();
        
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }
        
        RepositoryOLD::$db = $conn;
    }

    private function disconnect()
    {
        if (RepositoryOLD::$db != null)
            RepositoryOLD::$db->close();
    }

    private function retrieve_set($query, $paramSet)
    {
        $result_set = array();
        
        if (RepositoryOLD::$db == null)
            RepositoryOLD::init();
        
        $stmt = RepositoryOLD::$db->prepare($query);
        
        /* bind params to prepared statement */
        $this->bind_params($stmt, $paramSet);
        
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        
        $result_set = array();
        foreach ($result->fetch_all() as $array_obj) {
            array_push($result_set, $array_obj);
        }
        
        /* return */
        return $result_set;
    }

    private function fetch($query, $paramSet)
    {
        $result = $this->retrieve_set($query, $paramSet);
        if (count($result)) {
            return $result[0];
        } else {
            throw new NotFoundException("");
        }
    }

    private function execute($query, $paramSet)
    {
        if (RepositoryOLD::$db == null)
            RepositoryOLD::init();
        
        $stmt = RepositoryOLD::$db->prepare($query);
        
        //echo $query;
        //print_r($paramSet);
        
        /* bind params to prepared statement */
        $this->bind_params($stmt, $paramSet);

        
        /* execute */
        $stmt->execute();
        
        /* close */
        $stmt->close();
    }

    private function count($query, $paramSet)
    {
        if (RepositoryOLD::$db == null)
            RepositoryOLD::init();

        //echo $query;
        //se o statement for false, checar:
            //o nome da $_ENTITY está igual ao nome da tabela?
            
        $stmt = RepositoryOLD::$db->prepare($query);
        
        /* bind params to prepared statement */
        $this->bind_params($stmt, $paramSet);
        
        /* execute */
        $stmt->execute();
        
        /* bind results to prepared statement */
        $stmt->bind_result($count);
        
        /* fetch values */
        $stmt->fetch();
        
        /* close */
        $stmt->close();
        
        /* return */
        return $count;
    }

    private function bind_params($stmt, ParamSet $paramSet)
    {
        
        $types = $paramSet->getTypesString();
        
        $values = $paramSet->getValues();
        
        switch ($paramSet->size()) {
            case 0:
                break;
            case 1:
                $stmt->bind_param($types, $values[0]);
                break;
            case 2:
                $stmt->bind_param($types, $values[0], $values[1]);
                break;
            case 3:
                $stmt->bind_param($types, $values[0], $values[1], $values[2]);
                break;
            case 4:
                $stmt->bind_param($types, $values[0], $values[1], $values[2], $values[3]);
                break;
            case 5:
                $stmt->bind_param($types, $values[0], $values[1], $values[2], $values[3], $values[4]);
                break;
            case 6:
                $stmt->bind_param($types, $values[0], $values[1], $values[2], $values[3], $values[4], $values[5]);
                break;
            case 7:
                $stmt->bind_param($types, $values[0], $values[1], $values[2], $values[3], $values[4], $values[5], $values[6]);
                break;
        }
    }

    private function bind_results($stmt)
    {
        $resultSet = array();
        
        if ($stmt->affected_rows > 0)
            switch ($stmt->field_count) {
                case 1:
                    $stmt->bind_result($resultSet[0]);
                    break;
                case 2:
                    $stmt->bind_result($resultSet[0], $resultSet[1]);
                    break;
                case 3:
                    $stmt->bind_result($resultSet[0], $resultSet[1], $resultSet[2]);
                    break;
                case 4:
                    $stmt->bind_result($resultSet[0], $resultSet[1], $resultSet[2], $resultSet[3]);
                    break;
                case 5:
                    $stmt->bind_result($resultSet[0], $resultSet[1], $resultSet[2], $resultSet[3], $resultSet[4]);
                    break;
                case 6:
                    $stmt->bind_result($resultSet[0], $resultSet[1], $resultSet[2], $resultSet[3], $resultSet[4], $resultSet[5]);
                    break;
                case 7:
                    $stmt->bind_result($resultSet[0], $resultSet[1], $resultSet[2], $resultSet[3], $resultSet[4], $resultSet[5], $resultSet[6]);
                    break;
        }
        
        return $resultSet;
    }
}

