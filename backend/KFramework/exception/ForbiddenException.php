<?php
namespace exception;

class ForbiddenException extends KException
{

    protected function getType()
    {
        return "FORBIDDEN";
    }
}

