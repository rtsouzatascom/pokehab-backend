<?php
namespace exception;

abstract class KException extends \Exception
{
    
    private $msg;
    
    public function __construct($msg){
        $this->setInfo($msg);
    }
    
    public function getInfo(){
        return $this->getType().": ". $this->msg;
    }
    
    protected function setInfo($msg){
        $this->msg = $msg;
    }
    
    protected abstract function getType();
    
}

