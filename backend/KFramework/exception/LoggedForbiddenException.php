<?php
namespace exception;

class LoggedForbiddenException extends KException
{

    protected function getType()
    {
        return "FORBIDDEN";
    }
}

