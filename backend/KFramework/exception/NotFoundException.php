<?php
namespace exception;

class NotFoundException extends KException
{

    protected function getType()
    {
        return "NOT FOUND";
    }
}

