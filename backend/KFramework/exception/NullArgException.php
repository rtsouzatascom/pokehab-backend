<?php
namespace exception;

class NullArgException extends KException
{

    protected function getType()
    {
        return "NULL ARG";
    }
}

