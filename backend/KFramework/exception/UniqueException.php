<?php
namespace exception;

class UniqueException extends KException
{

    protected function getType()
    {
        return "UNIQUE";
    }
}

