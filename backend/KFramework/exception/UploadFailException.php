<?php
namespace exception;

class UploadFailException extends KException
{

    protected function getType()
    {
        return "UPLOAD FAIL";
    }
}

