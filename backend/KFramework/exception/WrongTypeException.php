<?php
namespace exception;

class WrongTypeException extends KException
{

    protected function getType()
    {
        return "WRONG TYPE";
    }
}

