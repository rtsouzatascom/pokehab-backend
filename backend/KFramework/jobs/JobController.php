<?php

class JobController {
    

    private $jobs = array();
    
    public function register(KJob $job) {
        array_push($this->jobs, $job);
    }
    
    public function execute() {
        foreach($this->jobs as $job)        
            $job->execute();
        
    }
}


