<?php

abstract class KJob
{

    private $params = [];

    public static $_API = "http://zeporteiro.com.br/api/control.php";

    public abstract function execute();

    protected function addParam($key, $value){
        $this->params[$key] = $value; 
    }
    
    public function submitTo($service_name) {
        $this->addParam('action', $service_name);

        $options = array(
            'http' => array(
                'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                "user_agent" => "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36",
                'method'  => 'POST',
                'content' => http_build_query($this->params)
            )
        );
        $result = file_get_contents(KJob::$_API, false, stream_context_create($options));
        if ($result === FALSE) { 
            error_log("Job [$service_name] falhou.");
        } else {
            error_log("Job [$service_name] executou com sucesso.");
        }
        
        var_dump($result);      
    }
}