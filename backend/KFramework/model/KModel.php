<?php
namespace model;

use DateTime;
use exception\NullArgException;
use JsonSerializable;

abstract class KModel implements JsonSerializable
{

    private $_ENTITY = "KModel";

    /**
     * Data de criação do objeto
     *
     * @property dt_create
     * @table model
     * @column dt_create
     * @notnull true
     * @type DateTime
     */
    private $dt_create;

    /**
     * Propriedade que refere-se a Data de criação do objeto
     *
     * @property dt_create
     * @table model
     * @column dt_create
     * @notnull true
     * @type DateTime
     */
    public static $_DT_CREATE = "dt_create";

    /**
     * Função para acessar Data de criação do objeto
     *
     * @author César França
     * @return DateTime dt_create
     * @version 6/18/2016 12:00:00 PM
     */
    public function getDtCreate()
    {
        return $this->dt_create;
    }

    /**
     * Função para configurar Data de criação do objeto
     *
     * @author César França
     * @param
     *            DateTime dt_create
     * @throws NullArgException
     * @version 6/18/2016 12:00:00 PM
     */
    public function setDtCreate($dt_create)
    {
        //if ($dt_create == null)
        //    throw new NullArgException(KModel::$_ENTITY . ".dt_create @ " . __FUNCTION__ . "is null");
        $this->dt_create = $dt_create;
    }

    /**
     * Função para checar se existe Data de criação do objeto
     *
     * @author César França
     * @return boolean
     * @version 6/18/2016 12:00:00 PM
     */
    public function hasDtCreate()
    {
        return ! empty($this->dt_create);
    }

    /**
     * Data de atualização do objeto
     *
     * @property dt_update
     * @table model
     * @column dt_update
     * @notnull true
     * @type DateTime
     */
    private $dt_update;

    /**
     * Propriedade que refere-se a Data de atualização do objeto
     *
     * @property dt_update
     * @table model
     * @column dt_update
     * @notnull true
     * @type DateTime
     */
    public static $_DT_UPDATE = "dt_update";

    /**
     * Função para acessar Data de atualização do objeto
     *
     * @author César França
     * @return DateTime dt_update
     * @version 6/18/2016 12:00:00 PM
     */
    public function getDtUpdate()
    {
        return $this->dt_update;
    }

    /**
     * Função para configurar Data de atualização do objeto
     *
     * @author César França
     * @param
     *            DateTime dt_update
     * @throws NullArgException
     * @version 6/18/2016 12:00:00 PM
     */
    public function setDtUpdate($dt_update)
    {
        //if ($dt_update == null)
          //  throw new NullArgException(KModel::$_ENTITY . ".dt_update @ " . __FUNCTION__ . "is null");
        $this->dt_update = $dt_update;
    }

    /**
     * Função para checar se existe Data de atualização do objeto
     *
     * @author César França
     * @return boolean
     * @version 6/18/2016 12:00:00 PM
     */
    public function hasDtUpdate()
    {
        return ! empty($this->dt_update);
    }

    /**
     * Data de exclusão do objeto
     *
     * @property dt_delete
     * @table model
     * @column dt_delete
     * @notnull true
     * @type DateTime
     */
    private $dt_delete;

    /**
     * Propriedade que refere-se a Data de exclusão do objeto
     *
     * @property dt_delete
     * @table model
     * @column dt_delete
     * @notnull true
     * @type DateTime
     */
    public static $_DT_DELETE = "dt_delete";

    /**
     * Função para acessar Data de exclusão do objeto
     *
     * @author César França
     * @return DateTime dt_delete
     * @version 6/18/2016 12:00:00 PM
     */
    public function getDtDelete()
    {
        return $this->dt_delete;
    }

    /**
     * Função para configurar Data de exclusão do objeto
     *
     * @author César França
     * @param
     *            DateTime dt_delete
     * @throws NullArgException
     * @version 6/18/2016 12:00:00 PM
     */
    public function setDtDelete($dt_delete)
    {
        //if ($dt_delete == null) throw new NullArgException(KModel::$_ENTITY . ".dt_delete @ " . __FUNCTION__ . "is null");
        $this->dt_delete = $dt_delete;
    }

    /**
     * Função para checar se existe Data de exclusão do objeto
     *
     * @author César França
     * @return boolean
     * @version 6/18/2016 12:00:00 PM
     */
    public function hasDtDelete()
    {
        return ! empty($this->dt_delete);
    }
                
    
    public function jsonSerialize()
    {
        return $this->toJSON();
        
    }
    
    public abstract function toJSON();

}

