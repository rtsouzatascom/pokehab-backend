<?php
namespace services;

abstract class Service
{
    public abstract function serviceName();
    
    public abstract function load(array $args); 
    public abstract function arg_list();
    
    protected function serializeCollection($list){
        $srl = array();
        foreach($list as $i){
            array_push($srl, $i);
        }
        return $srl;
    }
}

