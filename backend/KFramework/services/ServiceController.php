<?php
/*
$location = "../";
include_once $location."config.php";
Config::execute($location);
*/

use exception\KException;
use exception\NotFoundException;
use exception\NullArgException;
use exception\UniqueException;
use exception\WrongTypeException;
use services\Service;

class ServiceController {
    public static $_KEY = "action";

    private $services = array();
    
    public function register(Service $svc) {
        $key = $svc->serviceName();
        
        if(!array_key_exists($key, $this->services)){
            $this->services[$key] = $svc;
        }
    }
    
    public function load($svc_name) {
        
        $svc = $this->services[$svc_name];

        if($svc == null) error_log("service not found: ".$svc_name);

        $arg_keys = $svc->arg_list();
       
        $args = array();
        
        //unsing GET
        foreach ($arg_keys as $k){
            if(array_key_exists($k, $_GET)){
                $args[$k] = json_decode($_GET[$k], false);
            }
        }
        
        //using POST
        foreach ($arg_keys as $k){
            if(array_key_exists($k, $_POST)){
                $args[$k] = json_decode($_POST[$k], false);
            }
        }
        
        foreach ($arg_keys as $k){
            if(array_key_exists($k, $_FILES)){
                $args[$k] = $_FILES[$k];
            }
        }
        
        try {
         
            $this->services[$svc_name]->load($args);
            
        } catch (UniqueException $e) {
            ServiceMessage::returnError($e->getInfo(), ServiceMessage::$_ERROR_UNIQUE);
            
        } catch (NullArgException $e) {
            ServiceMessage::returnError($e->getInfo(), ServiceMessage::$_ERROR_NULL);
            
        } catch (NotFoundException $e) {
            ServiceMessage::returnError($e->getInfo(), ServiceMessage::$_ERROR_NOT_FOUND);
            
        } catch (WrongTypeException $e) {
            ServiceMessage::returnError($e->getInfo(), ServiceMessage::$_ERROR_WRONG_TYPE);
            
        } catch (KException $e) {
            ServiceMessage::returnError($e->getInfo(), ServiceMessage::$_ERROR_UNKNOWN);
            
        }
        
    }
}
?>


