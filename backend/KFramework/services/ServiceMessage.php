<?php

class ServiceMessage implements JsonSerializable {

    
    public static $_ERROR_UNIQUE = 900;
    public static $_ERROR_NULL = 901;
    public static $_ERROR_NOT_FOUND = 902;
    public static $_ERROR_WRONG_TYPE = 903;
    public static $_ERROR_UNKNOWN = 904;
    public static $_ERROR_USER_UNCONFIRMED = 905;
    public static $_ERROR_USER_ALREADY_CONFIRMED = 906;
    public static $_ERROR_WRONG_PASSWORD = 907;
    public static $_ERROR_WRONG_ARGS = 908;
    public static $_ERROR_USER_NO_INFO = 909;
    public static $_ERROR_UPLOAD_FAIL = 910;
    public static $_ERROR_USER_NOT_FOUND = 911;
    public static $_ERROR_CPF_JA_CADASTRADO = 912;
    public static $_ERROR_EMAIL_JA_CADASTRADO = 913;
    public static $_ERROR_REGISTRO_DUPLICADO = 914;
    public static $_ERRO_DEVICE_NOT_FOUND = 915;
    
    public static $_FAIL = -1;
    public static $_ERROR = -2;
    public static $_SUCCESS = 1;
    
    private $type;
    public static  $_TYPE = 'type';
    
    private $msg;
    public static  $_MSG = 'msg';
    
    private $obj;
    public static  $_OBJ = 'obj';
    
    private $err;
    public static  $_ERR = 'err';
    
    private $tms;
    public static  $_TMS = 'tms';
    
    public function __construct( $type,  $msg=null){
        $this->type = $type;
        
        if(!isset($msg)) $msg = "";
        $this->msg = $msg;

        $this->tms = date("Y-m-d H:i:s");
    }
    
    public static function returnSuccess(){
        $sm = new ServiceMessage(ServiceMessage::$_SUCCESS);
        print $sm->jsonSerialize();
    }
    
    public static function returnError($e, $err){
        $sm = new ServiceMessage(ServiceMessage::$_ERROR, $e);
        $sm->err = $err;
        print $sm->jsonSerialize();
    }
    
    public static function returnFail($e){
        $sm = new ServiceMessage(ServiceMessage::$_FAIL, $e);
        print $sm->jsonSerialize();
    }
    
    public static function returnObject($obj){
        
        $sm = new ServiceMessage(ServiceMessage::$_SUCCESS, "sucess");
        $sm->obj = $obj;
        
        print $sm->jsonSerialize();
       // print $obj->toJSON();
    }
    
    /**
     * Função para gerar um objeto a partir de um JSON
     *
     * @author César França
     * @return ServiceMessage
     * @version 6/22/2016
     */
    public static function fromJSON($stdObj) {

        $sm = new ServiceMessage('');
        if(property_exists($stdObj, ServiceMessage::$_TYPE)) $sm->type = $stdObj->type;
        if(property_exists($stdObj, ServiceMessage::$_MSG)) $sm->msg = $stdObj->msg;
        if(property_exists($stdObj, ServiceMessage::$_OBJ)) $sm->obj = $stdObj->obj;
        if(property_exists($stdObj, ServiceMessage::$_ERR)) $sm->err = $stdObj->err;
        if(property_exists($stdObj, ServiceMessage::$_TMS)) $sm->tms = $stdObj->tms;
        return $sm;
        
    }
    
    /**
     * Função para transformar o objeto em JSON
     *
     * @author César França
     * @version 6/22/2016
     */
    public function jsonSerialize() {
        $arr_obj = array();
        $arr_obj[ServiceMessage::$_TYPE] = json_encode($this->type);
        $arr_obj[ServiceMessage::$_MSG] = json_encode($this->msg);
        $arr_obj[ServiceMessage::$_ERR] = json_encode($this->err);
        $arr_obj[ServiceMessage::$_TMS] = json_encode($this->tms);

        
        
        //print_r("antes:\n".$this->obj."\n\n");
        
        if( method_exists($this->obj, "toJSON") ){
            $arr_obj[ServiceMessage::$_OBJ] = $this->obj->toJSON();
           
        } else {
            $arr_obj[ServiceMessage::$_OBJ] = $this->obj;
        }

        //print_r("depois:\n".$arr_obj[ServiceMessage::$_OBJ]."\n\n");
        
        
        /*
        if($this->obj instanceof KModel){
        } else {
            $arr_obj[ServiceMessage::$_OBJ] = json_encode($this->obj);
        }*/
        
        $result = json_encode($arr_obj);
        
        /*
        $result = str_replace("\"{","{",$result);
        $result = str_replace("\\","",$result);
        $result = str_replace("}\"","}",$result);
        */
        
        return $result;
        
    }

    
}



































