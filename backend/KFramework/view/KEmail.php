<?php

abstract class KEmail
{

    public static $_DOMAIN = "zeporteiro.com.br";

    public static $_ROOT = "/";

    private $to;

    private $subject;

    private $content;
    
    private $from = "suporte@zeporteiro.com.br";

    protected function setTo($to)
    {
        $this->to = $to;
    }

    protected function setSubject($subject)
    {
        $this->subject = $subject;
    }

    protected function setContent($content)
    {
        $this->content = $content;
    }

    protected function send()
    {
        $headers = "From: ".$this->from." \n Reply-To: ".$this->from." \n" ."Return-Path: ".$this->from." \n"  .'MIME-Version: 1.0' . "\n" . 'Content-type: text/html; charset=UTF-8' . "\n" . "Content-Transfer-Encoding: base64"."\n" . 'X-Mailer: PHP/' . phpversion();
        if(!mail($this->to,$this->subject, $this->content, $headers)){ // Se for Postfix
            // $headers .= "Return-Path: " . $emailsender . $quebra_linha; // Se "não for Postfix"
           // mail($emaildestinatario, $assunto, $mensagemHTML, $headers );
         }
       
        
     //  $email= mail($this->to, $this->Email_Encode($this->subject, 'UTF-8'), chunk_split(base64_encode($this->content)), $headers, "-r", $from);
    //    if($email){
    //        print_r("teste");
    //    }else{
    //     print_r("teste");
    //    }
    }

    protected abstract function getSignature();
    
    private function Email_Encode($String = '', $Caracteres = 'ISO-8859-1')
    {
        // Quoted-printed (Q)
        if (function_exists('quoted_printable_encode')) {
            $String = quoted_printable_encode($String);
            $RT = '=?' . $Caracteres . '?Q?' . $String . '?=';
            
            // IMAP 8bit (Q)
        } else If (function_exists('imap_8bit')) {
            $String = imap_8bit($String);
            $RT = '=?' . $Caracteres . '?Q?' . $String . '?=';
        } Else        // Base64 (B)
        {
            $String = base64_encode($String);
            $RT = '=?' . $Caracteres . '?B?' . $String . '?=';
        }
        
        Return $RT;
    }

    public abstract function execute();
}