<?php
namespace view;

use ServiceController;
use ServiceMessage;

abstract class KPage {
    
    public static $_VISIBILITY_EVERYONE = -1;
    public static $_VISIBILITY_NOT_LOGGED_ONLY = 0;
    public static $_VISIBILITY_LOGGED_ONLY = 1;
    
    public static function pageName() {}
    
    public static function arg_list() {}
    
    public static function load($args) {} 
    
    public static function visibility() {}
    
    public static function isLogged() {}
    
    public static function scripts(){}
    
    public static function kscripts(){
        ?>

        <script type="text/javascript">

		function id (who) {
			return document.getElementById(who);
		}

        
        var httpRequest;

        function request(svc, args, func_result, func_error) {
        	  		  
        	if (window.XMLHttpRequest) { // Mozilla, Safari, ...
        		httpRequest = new XMLHttpRequest();
        	} else if (window.ActiveXObject) { // IE
        	  try {
        	    httpRequest = new ActiveXObject("Msxml2.XMLHTTP");
        	  } 
        	  catch (e) {
        	    try {
        	      httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        	    } 
        	    catch (e) {}
        	  }
        	}
        	
        	if (!httpRequest) {
        		janela_mensagem('Giving up :( Cannot create an XMLHTTP instance');
        		return false;
        	}
        	
        	httpRequest.onreadystatechange = function () {
                if (httpRequest.readyState == 4 && httpRequest.status === 200){

					//PRINT STATUS
                	//console.log(httpRequest.responseText);

					var return_msg = JSON.parse(httpRequest.responseText);
					if(return_msg.type == <?php echo ServiceMessage::$_SUCCESS?>){
                		func_result(return_msg);
					} else {
						func_error(return_msg);
					}

                } 
        	};
        	
        	httpRequest.open('POST', 'control.php');
        	httpRequest.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        	httpRequest.send( service(svc) + args);
        }
                        
        function service(func) {
        	return "<?php echo ServiceController::$_KEY?>="+func+"&";
        }

        function status(lbl){
    		id("status").innerHTML=lbl;
    	}

        function link(page){
        	window.location.replace("index.php?<?php echo PageController::$_KEY?>="+page);
            //$.redirect("index.php", { '<?php echo PageController::$_KEY?>': page } );
         }

        function link_send(page, args){
        	//args.<?php echo PageController::$_KEY?> = page;
            //$.redirect("index.php", args );
			window.location.replace("index.php?<?php echo PageController::$_KEY?>="+page+"&"+argsURL(args));
         }

		function argsURL(args){
			
		    var keys = objKeys(args);
			var values= objValues(args);

			var url = "";
			for(var i=0; i<keys.length;i++){
			   url += encodeURIComponent(keys[i]) + "=" + values[i] + "&";
			}

			return url;
		}
		function objKeys(obj){
			var keys=[]
			$.each(obj, function(i,n) {
			    keys.push(i);});
		    return keys;
		}
		function objValues(obj){
			var vals=[]
			$.each(obj, function(i,n) {
				vals.push(n);});
		    return vals;
		}
        
        //FUNÇÔES PARA MOSTRAR MENSAGENS EM JANELAS MODAIS 
		function janela_mensagem(titulo, conteudo){
			id("titulo-modal").innerHTML = titulo;
        	id("conteudo-modal").innerHTML = conteudo;
        	$('#janelaMensagem').modal('show');
			
		}

		function janela_mensagem_redirect(titulo, conteudo, destino){

			id("titulo-modal").innerHTML = titulo;
        	id("conteudo-modal").innerHTML = conteudo;

        	var btn = id("button-modal");
        	var action = "link('"+destino+"');"
        	btn.setAttribute("onclick", action);
        	
        	$('#janelaMensagem').modal('show');
			
		}

		function janela_mensagem_reload(titulo, conteudo){

			id("titulo-modal").innerHTML = titulo;
        	id("conteudo-modal").innerHTML = conteudo;

        	var btn = id("button-modal");
        	var action = "window.location.reload();"
        	btn.setAttribute("onclick", action);
        	
        	$('#janelaMensagem').modal('show');
			
		}

		//FUNÇÕES PARA AUXILIAR NA INTERFACE DA VALIDAÇÃO DE FORMULARIOS
		function validateEmail(mail){
		    return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail));
		}

		function findChild(baseElement, wantedElementID) {
		    var elementToReturn;
		    var length = baseElement.childNodes.length;
		    for (var i = 0; i < length; i++) {
		        elementToReturn = baseElement.childNodes[i];
		        if (elementToReturn.id == wantedElementID) {
		            return elementToReturn;
		        } else {
		        	elementToReturn = findChild(elementToReturn, wantedElementID);
					if(elementToReturn) { 
						return elementToReturn; 
					} 
		        }
		    }
		}

		function controle_campo(form_id, input_id, help_id, is_valido, msg){
			var form = id(form_id);

			var input = findChild(form,input_id);
			
			var help = findChild(form,help_id);

			if(is_valido){
		    	input.setAttribute("class", "form-control is-valid");
		    	help.setAttribute("class", "valid-feedback");
			} else {
				input.setAttribute("class", "form-control is-invalid");
		    	help.setAttribute("class", "invalid-feedback");
			}

			help.innerHTML = msg;
			
		}       

		function controle_botao_formulario(is_valid, btn){
			if(is_valid){
				btn.removeAttribute("disabled");
				btn.setAttribute("class", "btn btn-primary");
			} else {
				btn.setAttribute("disabled", "true");
				btn.setAttribute("class", "btn btn-secondary");
			}
			
		}

		function getExtension(filename) {
		    var parts = filename.split('.');
		    return parts[parts.length - 1];
		}

		function isImage(filename) {
		    var ext = getExtension(filename);
		    switch (ext.toLowerCase()) {
		    case 'jpg':
		    case 'jpeg':
		    case 'gif':
		    case 'png':
		        return true;
		    }
		    return false;
		}

		function anulaTeclaEnter(obj_id){
			id(obj_id).onkeypress = function(e){
	    	    if(e.which == 13) {
		    		e.preventDefault();
	    	    }
	    	  };			
		}

		function substituiTeclaEnter(obj_id, func){
			id(obj_id).onkeypress = function(e){
	    	    if(e.which == 13) {
		    		e.preventDefault();
		    		func();
	    	    }
	    	  };			
		}

		function hide(field){
			field.setAttribute("hidden","hidden");
		}
		function show (field){
			field.removeAttribute("hidden");
		}
        </script>    
    
    
        <!-- Modal MSG -->
        <div class="modal fade" id="janelaMensagem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
        <h5 class="modal-title" id="titulo-modal"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div id='conteudo-modal' class="modal-body">
        
        </div>
        <div class="modal-footer">
        <button id="button-modal" type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
        </div>
        </div>
        </div>
        
        
    <?php         
    }
}

