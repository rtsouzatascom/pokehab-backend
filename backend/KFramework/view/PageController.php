<?php
namespace view;

use exception\ForbiddenException;
use exception\LoggedForbiddenException;

class PageController {
    public static $_KEY = "c21199437976e2b3bbc1e3f78c33110f";
    
    private $pages = array();
    
    public function register(KPage $page) {
        $key = $page->pageName();
        
        if(!array_key_exists($key, $this->pages)){
            $this->pages[$key] = $page;
        }
    }
    
    public function load(string $page_name) {
        
        $pgs = $this->pages[$page_name];
        $page = $this->pages[$page_name];
        $visibility = $page->visibility();
        $logged = $page->isLogged();
        
        //CHECA A PERMISSAO DE ACESSO
        $allowed = false;
        switch ($visibility){
            case KPage::$_VISIBILITY_EVERYONE:
                $allowed = true;
                break;
                
            case KPage::$_VISIBILITY_LOGGED_ONLY:
                $allowed = $logged;
                break;
                
            case KPage::$_VISIBILITY_NOT_LOGGED_ONLY:
                $allowed = !($logged);
                break;
        }
        
        //SE ESTIVER PERMITIDO
        if($allowed){
            
            $arg_keys = $page->arg_list();
            $args = array();
            
            foreach ($arg_keys as $k){
                if(array_key_exists($k, $_GET)){
                    $args[$k] = $_GET[$k];
                }
            }
            
            $page->kscripts();
            $page->scripts();
            $page->load($args);
            
        }else {
            
            if($logged) {
                throw new LoggedForbiddenException("");
            } else {
                throw new ForbiddenException("");
            }
        }
        
        
    }
}


