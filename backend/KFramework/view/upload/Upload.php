<?php
use exception\UploadFailException;

/*
 Desenvolvido por Marco Antoni <marquinho9.10@gmail.com>
 */

class Upload{

    public static $_ERRO_TAMANHO = -1;
    public static $_ERRO_UNKNOWN = -2;
    public static $_SUCESSO = 1;
    
    protected $arquivo;
    protected $altura;
    protected $largura;
    protected $identificador;
    protected $novoNome;
    protected $extensao;
    
    function __construct($identificador, $arquivo){
        $this->arquivo = $arquivo;
        $this->identificador = $identificador;
        $this->altura  = Config::$_PHOTO_HEIGHT;
        $this->largura = Config::$_PHOTO_WIDTH;

    }

    public function setDimensoes( $altura, $largura){
        $this->altura  = $altura;
        $this->largura = $largura;
    }
    
    public function setExtensao($ext){
        $this->extensao = $ext;
    }

    protected function getExtensao(){
        $nome = $this->arquivo['name'];
        return pathinfo($nome, PATHINFO_EXTENSION);
    }
    
    protected function ehImagem($extensao){
        $extensoes = array('gif', 'jpeg', 'jpg', 'png');     // extensoes permitidas
        if (in_array($extensao, $extensoes))
            return true;
    }
    
    /**
     * ORIGINAL
     */
    /*
    //largura, altura, tipo, localizacao da imagem original
    protected function redimensionar($imgLarg, $imgAlt, $tipo, $img_localizacao){
        
            //redimencionar a imagem
            $novaLarg = $this->largura;
            $novaAlt = $this->altura;
        
            //cria uma nova imagem com o novo tamanho
            $novaimagem = imagecreatetruecolor($novaLarg, $novaAlt);
            
            switch ($tipo){
                case 1: // gif
                    $origem = imagecreatefromgif($img_localizacao);
                    imagecopyresampled($novaimagem, $origem, 0, 0, 0, 0,
                        $novaLarg, $novaAlt, $imgLarg, $imgAlt);
                    imagegif($novaimagem, $img_localizacao);
                    break;
                case 2: // jpg
                    $origem = imagecreatefromjpeg($img_localizacao);
                    imagecopyresampled($novaimagem, $origem, 0, 0, 0, 0,
                        $novaLarg, $novaAlt, $imgLarg, $imgAlt);
                    imagejpeg($novaimagem, $img_localizacao);
                    break;
                case 3: // png
                    $origem = imagecreatefrompng($img_localizacao);
                    imagecopyresampled($novaimagem, $origem, 0, 0, 0, 0,
                        $novaLarg, $novaAlt, $imgLarg, $imgAlt);
                    imagepng($novaimagem, $img_localizacao);
                    break;
            }
            
            //destroi as imagens criadas
            imagedestroy($novaimagem);
            imagedestroy($origem);
    }
    */

    /**
     * MODIFICADO
     */
    //largura, altura, tipo, localizacao da imagem original
    protected function redimensionar($imgLarg, $imgAlt, $tipo, $img_localizacao){

            //error_log("\n (0) imgLarg=$imgLarg, imgAlt=$imgAlt");

            $this->setDimensoes( 500, 500);
            $novaLarg = $imgLarg;
            $novaAlt = $imgAlt;

            //error_log("\n (1) novaLarg=$novaLarg, novaAlt=$novaAlt");

            //redimencionar a imagem
            if($imgLarg > $this->largura){
                $proporcao = ($this->largura / $imgLarg);
                $novaLarg = $proporcao * $novaLarg;
                $novaAlt = $proporcao * $novaAlt;
            }

            //error_log("\n (2) novaLarg=$novaLarg, novaAlt=$novaAlt");            

            if($novaAlt > $this->altura){
                $proporcao = ($this->altura / $novaAlt);
                $novaLarg = $proporcao * $novaLarg;
                $novaAlt = $proporcao * $novaAlt;
            }
        
            //error_log("\n (3) novaLarg=$novaLarg, novaAlt=$novaAlt");            

            //cria uma nova imagem com o novo tamanho
            $novaimagem = imagecreatetruecolor($novaLarg, $novaAlt);
            
            switch ($tipo){
                case 1: // gif
                    $origem = imagecreatefromgif($img_localizacao);
                    imagecopyresampled($novaimagem, $origem, 0, 0, 0, 0,
                        $novaLarg, $novaAlt, $imgLarg, $imgAlt);
                    imagegif($novaimagem, $img_localizacao);
                    break;
                case 2: // jpg
                    $origem = imagecreatefromjpeg($img_localizacao);
                    imagecopyresampled($novaimagem, $origem, 0, 0, 0, 0,
                        $novaLarg, $novaAlt, $imgLarg, $imgAlt);
                    imagejpeg($novaimagem, $img_localizacao);
                    break;
                case 3: // png
                    $origem = imagecreatefrompng($img_localizacao);
                    imagecopyresampled($novaimagem, $origem, 0, 0, 0, 0,
                        $novaLarg, $novaAlt, $imgLarg, $imgAlt);
                    imagepng($novaimagem, $img_localizacao);
                    break;
            }
            
            //destroi as imagens criadas
            imagedestroy($novaimagem);
            imagedestroy($origem);
    }

    public function salvar($pasta){
        $extensao = $this->extensao;
        if($extensao == null){
            $extensao = Config::$_PHOTO_EXTENSION;
        }
        
        //gera um nome unico para a imagem em funcao do tempo
        $this->novoNome = $this->identificador . "_" . time() . '.' . $extensao;
        
        //localizacao do arquivo
        $destino = $pasta . $this->novoNome;
        
        if ($this->ehImagem($extensao)){

            
  //pega a largura, altura, tipo e atributo da imagem
            list($largura, $altura, $tipo, $atributo) = getimagesize($this->arquivo['tmp_name']);

             // testa se é preciso redimensionar a imagem
              if(($largura > $this->largura) || ($altura > $this->altura))
                $this->redimensionar($largura, $altura, $tipo, $destino);
            //move o arquivo normal
             if (! move_uploaded_file($this->arquivo['tmp_name'], $destino)){
                
            //     if ($this->arquivo['error'] == 1)
            //     throw new UploadFailException("ERRO TAMANHO");
            //     else
            //     throw new UploadFailException("ERRO DESCONHECIDO ". "(" . $this->arquivo['error'] . ")");
             }
            
          
        }
        
        return true;
    }
    
    public function salvarMiniatura($pasta){
        $extensao = $this->extensao;
        if($extensao == null){
            $extensao = Config::$_PHOTO_EXTENSION;
        }
        
        //gera um nome unico para a imagem em funcao do tempo
        $this->novoNome = "mini_" . $this->identificador . "_" . time() . '.' . $extensao;
        
        //localizacao do arquivo
        $destino = $pasta . $this->novoNome;
        
        if ($this->ehImagem($extensao)){
            
            //move o arquivo normal
             if (! move_uploaded_file($this->arquivo['tmp_name'], $destino)){
                
            //     if ($this->arquivo['error'] == 1)
            //     throw new UploadFailException("ERRO TAMANHO");
            //     else
            //     throw new UploadFailException("ERRO DESCONHECIDO ". "(" . $this->arquivo['error'] . ")");
             }
            
            $this->redimensionar(Config::$_PHOTO_MINI_WIDTH, Config::$_PHOTO_MINI_HEIGHT, $tipo, $destino);
        }
        
        return true;
    }

    public function getNovoNome(){
        return $this->novoNome;
    }
}
?>