<?php
use exception\UploadFailException;

class UploadBlob extends Upload{

    public function salvarBlob($pasta, $blob){
        $extensao = $this->extensao;
        error_log("extensao=".$extensao);
        
        //gera um nome unico para a imagem em funcao do tempo
        $this->novoNome = $this->identificador . "_" . time() . '.' . $extensao;
        
        //localizacao do arquivo
        $destino = $pasta . $this->novoNome;
        
        if ($this->ehImagem($extensao)){
            
            //move o arquivo
            if (! file_put_contents($destino, $blob)){
                
                if ($this->arquivo['error'] == 1)
                    throw new UploadFailException("ERRO TAMANHO");
                    else
                        throw new UploadFailException("ERRO DESCONHECIDO ". "(" . $this->arquivo['error'] . ")");
            }
            
            //pega a largura, altura, tipo e atributo da imagem
            list($largura, $altura, $tipo, $atributo) = getimagesize($destino);
            
            $this->redimensionar($largura, $altura, $tipo, $destino);
        }
        
        return true;
    }
}
?>