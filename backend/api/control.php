<?php

if (!headers_sent()) {
    header('Access-Control-Allow-Headers: "Origin, X-Requested-With, Content-Type, Accept"');
    header("Access-Control-Allow-Origin: *", false);
    header('content-type: application/json; charset=utf-8');
    session_start();
}

include_once "config.php";
include_once "../KFramework/services/Service.php";
include_once "../KFramework/services/ServiceController.php";
Config::execute("");
$ctrl = new ServiceController();
foreach(Config::$_SERVICES as $c){
    $c = "services\\".$c;
    $ctrl->register(new $c());
}
$ctrl->load($_POST[ServiceController::$_KEY]);


?>
