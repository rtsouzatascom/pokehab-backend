<?php

namespace services;
use ServiceMessage;
use exception\KException;

abstract class PKService extends Service{

    

    private static $DAOLog;

    private $filename;

    private $DAOImagem;

    private $args;



    public function serviceName(){

 

        return $this->ZP_serviceName();

    }



    //ID_USUARIO é um argumento que sempre deverá ser enviado para os serviços ZéPorteiroService

    public function arg_list() {

        $arg_list = $this->ZP_arg_list();


        return $arg_list;

    }   

    

    public function read($arg_name){

  

        $key = $arg_name;
        $value = $this->args[$key];
        
        return $value;

    }



    public function load(array $params)

    {

        

        //configura o mecanismo de Log

        if($this->ZP_debug()){

            $this->filename = "../api/services/".substr(strrchr(get_class($this), "\\"), 1).".log";

            error_log("checar arquivo: ". $this->filename);

        }

        $this->log($this->ZP_serviceName(), " STARTED ");

        

        //recupera os argumentos

        $this->args = $params;

        

       

     // $id_usuario = $args[Usuario::$_ID_USUARIO];



        try {



            //neste local => verificar se o usuário ID_USUARIO tem permissao para acessar o serviço ZP_SERVICE_NAME



            $this->ZP_load();




            

        } catch (KException $e){

            




            ServiceMessage::returnFail($e->getMessage());

        

        }

        

    }

    

    protected function log($label, $content){

        if($this->ZP_debug()){

            $date = date("Y-m-d H:i:s");

            $content_str = print_r($content, true);

            error_log("\n[".$date."] ".$label." = ".$content_str, 3, $this->filename);

        } 

    }



    private function gravarLog($id_usuario, $descricao){



        if(PKService::$DAOLog == null)

        PKService::$DAOLog = new DAOLog();

            

        $log = new Log();

        $log->setIdUsuario($id_usuario);

        $log->setDsLog($descricao);

            

        PKService::$DAOLog->create($log);

        

    }

    

    /*************************************

     * MÉTODOS PARA REALIZAR NOTIFICAÇÃO

     */

   

    //MÉTODOS QUE DEVEM SER IMPLEMENTADOS PELOS SERVIÇOS ZÉPORTEIRO

    public abstract function ZP_serviceName();



    public abstract function ZP_arg_list();

    

    public abstract function ZP_debug();



    public abstract function ZP_load();





}





?>

