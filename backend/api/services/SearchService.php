<?php
namespace services;
use ServiceMessage;
use exception\KException;

class SearchService extends PKService{
    
    private static $_KEY = "SearchService";
    
    public function ZP_serviceName(){
        return SearchService::$_KEY;
    }
    
    public function ZP_debug(){
        return false; //indica se o serviço deve imprimir log no servidor ou nao (usar apenas quando em modo de desenvolvimento/debug)
    }

    public function ZP_arg_list() {
        return ["pokemon"];
    }
    
    public function ZP_load()
    {
        $pokemon = $this->read("pokemon");
    
        $ch = curl_init();
        $getUrl = "https://pokeapi.co/api/v2/pokemon/".$pokemon;
        curl_setopt($ch, CURLOPT_URL, $getUrl);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        $result = curl_exec($ch);
       

        ServiceMessage::returnObject($result); 
        
    }
    
    
    
}


?>